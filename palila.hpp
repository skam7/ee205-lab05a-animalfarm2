///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.hpp
/// @version 1.0
///
/// Exports data about all palila
///
/// @author Shannon Kam <skam7@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   02_18_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include <string>

#include "bird.hpp"

using namespace std;


namespace animalfarm {

class Palila : public Bird {
public:
	string whereFound;
	
	Palila( string found, enum Color newColor, enum Gender newGender );
	
	//virtual const string speak();

	void printInfo();
};

} // namespace animalfarm
